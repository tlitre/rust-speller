#![feature(test)]

use std::borrow::Cow;
use std::io::{BufRead, BufReader};

extern crate test;

use correct_lib::CorrectorModel;
use spell_bench::{Correction, Corrector, DefaultTokenizer, Tokenizer};

struct Model(CorrectorModel);

impl Corrector for Model {
    type Tokens = DefaultTokenizer;

    fn from_corpus<R: BufRead>(corpus: R) -> Self {
        let model = DefaultTokenizer::tokenize(corpus).iter().collect();
        Model(model)
    }

    fn suggest(&self, word: &str) -> Correction {
        use correct_lib::Correction as MyCorrection;
        use Correction::*;
        match self.0.suggest(word) {
            MyCorrection::Correct => Correct,
            MyCorrection::Incorrect => Uncorrectable,
            MyCorrection::Suggestion(w) => Suggestion(Cow::from(w)),
        }
    }
}

#[test]
fn it_works() {
    let model = Model::from_corpus(BufReader::new(&b"hello world"[..]));
    assert_eq!(
        Correction::Suggestion(Cow::from("world")),
        model.suggest("word")
    )
}

#[bench]
fn bench(bench: &mut test::Bencher) {
    let model = Model::from_corpus(BufReader::new(&b"hello world"[..]));
    bench.iter(|| {
        model.suggest("world");
    });
}

spell_bench::spell_bench! {
    mod benches {
        const N: usize = 50;
        use super::Model as Corrector;
        bench_corrector!();
        use spell_bench::Edit;

        mod deletions {
            use super::*;
            //edit_hamlet!(first_1: Edit::delete(0));
            //edit_hamlet!(first_2: Edit::delete(0).then(Edit::delete(0)));
            //edit_hamlet!(last_1: Edit::delete(-2));
            //edit_hamlet!(del_last_2: Edit::delete(-2).then(Edit::delete(-2)));
            //edit_hamlet!(del_first_last: Edit::delete(0).then(Edit::delete(-2)));
        }
    }
}
