// Assumptions made in this implementation:
//     * This implementation can handle any unicode characters
//     * Splitting ascii seperately should help performance for latin text
//     *

use std::cmp::Ordering;
use std::collections::{BTreeMap, HashMap};
use std::iter::FromIterator;

mod metric;
pub use metric::calculate_distance;

/// CorrectorModel contains the words and frequencies learned
#[derive(Debug, PartialEq)]
pub struct CorrectorModel {
    freq_by_len: BTreeMap<usize, HashMap<String, usize>>,
    pub max_edits: usize,
}

impl Default for CorrectorModel {
    fn default() -> Self {
        Self::with_max_edits(2)
    }
}

impl CorrectorModel {
    /// Initializes an empty CorrectorModel with default max_edits value of 2
    pub fn new() -> Self {
        Self::default()
    }

    pub fn with_max_edits(max_edits: usize) -> Self {
        Self {
            freq_by_len: BTreeMap::new(),
            max_edits,
        }
    }

    /// Adds a single instance of the word to the frequency dict of this model
    pub fn learn(&mut self, word: &str) {
        let word = word.to_lowercase();
        *self
            .freq_by_len
            .entry(word.chars().count())
            .or_default()
            .entry(word)
            .or_default() += 1;
    }

    /// Suggests best candidate based on distance then frequency
    ///
    /// The best suggestion is chosen based on the following creteria:
    ///
    /// 1. Minimum edit distance;
    /// 2. Highest frequency if distance is tied.
    // It may make more sense to take in max_edits here but I'll follow the spec
    pub fn suggest(&self, cand: &str) -> Correction {
        let cand = cand.to_lowercase();
        let c_len = cand.chars().count() as usize;
        if self
            .freq_by_len
            .get(&c_len)
            .map(|words| words.contains_key(&cand))
            == Some(true)
        {
            return Correction::Correct;
        }

        let mut best = PossibleSuggestion::new(self.max_edits);
        for (word, &freq) in self
            .freq_by_len
            .range(c_len.saturating_sub(self.max_edits)..=c_len.saturating_add(self.max_edits))
            .map(|(_, words)| words)
            .flatten()
        {
            let d = calculate_distance(&cand, word);
            if Ordering::Less
                == d.cmp(&best.distance)
                    .then_with(|| best.frequency.cmp(&freq))
            {
                best = PossibleSuggestion::from(word, freq, d);
            }
        }

        if best.distance == self.max_edits + 1 {
            Correction::Incorrect
        } else {
            Correction::Suggestion(best.word)
        }
    }
}

/// PossibleSuggestion holds best possibilities for suggestion iteration
// Could be used to return a list of valid suggestions instead
#[derive(Debug, PartialEq)]
struct PossibleSuggestion<'a> {
    word: &'a str,
    frequency: usize,
    distance: usize,
}

impl<'a> PossibleSuggestion<'a> {
    /// Initializes a PossibleSuggestion with default values
    fn new(max_edits: usize) -> Self {
        PossibleSuggestion {
            word: "",
            frequency: std::usize::MAX,
            distance: max_edits + 1,
        }
    }

    /// Makes a PossibleSuggestion from given values
    fn from(w: &'a str, f: usize, d: usize) -> Self {
        PossibleSuggestion {
            word: w,
            frequency: f,
            distance: d,
        }
    }
}

/// Implements FromIterator so CorrectorModel can be returned from an iterator
impl<R: AsRef<str>> FromIterator<R> for CorrectorModel {
    fn from_iter<I>(it: I) -> Self
    where
        I: IntoIterator<Item = R>,
    {
        let mut model = Self::new();
        for word in it {
            model.learn(word.as_ref());
        }
        model
    }
}

/// A Correction is a possible return from a call to suggest
#[derive(Debug, PartialEq)]
pub enum Correction<'a> {
    Correct,
    Incorrect,
    Suggestion(&'a str),
}

#[cfg(test)]
mod suggest_struct_tests {
    use super::*;

    #[test]
    fn make_new_ps() {
        assert_eq!(
            PossibleSuggestion::new(2),
            PossibleSuggestion {
                word: "",
                frequency: std::usize::MAX,
                distance: 3
            }
        )
    }

    #[test]
    fn makes_from_ps() {
        assert_eq!(
            PossibleSuggestion::from("hello", 100, 1),
            PossibleSuggestion {
                word: "hello",
                frequency: 100,
                distance: 1,
            }
        )
    }

}

#[cfg(test)]
mod model_tests {
    use super::*;

    fn make_dummy_model() -> CorrectorModel {
        let mut model = CorrectorModel::new();
        for &s in &["foo", "foo", "foo", "foobar", "bar"] {
            model.learn(s);
        }
        model
    }

    fn make_dummy_with(added: Vec<(String, usize)>) -> CorrectorModel {
        let mut model = make_dummy_model();
        for (w, f) in added {
            let w = w.to_lowercase();
            let inc = model
                .freq_by_len
                .entry(w.chars().count())
                .or_default()
                .entry(w)
                .or_insert(0);
            *inc += f;
        }
        model
    }

    #[test]
    fn make_new_cm() {
        assert_eq!(
            CorrectorModel::new(),
            CorrectorModel {
                freq_by_len: BTreeMap::new(),
                max_edits: 2,
            }
        )
    }

    #[test]
    fn learn_word() {
        let mut model = CorrectorModel::new();
        model.learn("foobar");
        let mut hm = HashMap::new();
        hm.insert(String::from("foobar"), 1);
        let mut btm = BTreeMap::new();
        btm.insert(6, hm);
        assert_eq!(model.freq_by_len, btm);
    }

    #[test]
    fn learn_corpus() {
        let model = make_dummy_model();
        let btm = vec![(3, vec![("foo", 3), ("bar", 1)]), (6, vec![("foobar", 1)])]
            .into_iter()
            .map(|(len, items)| {
                (
                    len,
                    items
                        .into_iter()
                        .map(|(w, f)| (String::from(w), f))
                        .collect::<HashMap<_, _>>(),
                )
            })
            .collect::<BTreeMap<_, _>>();
        assert_eq!(model.freq_by_len, btm);
    }

    #[test]
    fn learn_capitalized() {
        let mut model = make_dummy_model();
        model.learn("FOOBAR");
        let dummy = make_dummy_model();
        assert_eq!(
            dummy.freq_by_len[&6]["foobar"] + 1,
            model.freq_by_len[&6]["foobar"],
        );
    }

    #[test]
    fn candidate_in_corpus() {
        let model = make_dummy_model();
        assert_eq!(model.suggest("bar"), Correction::Correct);
    }

    #[test]
    fn no_suitable_correction() {
        let model = make_dummy_model();
        assert_eq!(model.suggest("spaghetti"), Correction::Incorrect);
    }

    #[test]
    fn one_possible_correction() {
        let model = make_dummy_model();
        assert_eq!(model.suggest("foobarr"), Correction::Suggestion("foobar"));
    }

    #[test]
    fn higher_freq_correction() {
        let added = vec![(String::from("barn"), 20), (String::from("bard"), 40)];
        let model = make_dummy_with(added);
        assert!(
            model
                .freq_by_len
                .get(&4)
                .and_then(|words| words.get("barn"))
                > model.freq_by_len.get(&3).and_then(|words| words.get("bar")),
        );
        assert_eq!(model.suggest("bark"), Correction::Suggestion("bard"));
    }

    #[test]
    fn two_edits_away() {
        let model = make_dummy_model();
        assert_eq!(model.suggest("f"), Correction::Suggestion("foo"));
    }

    #[test]
    fn three_edits_away() {
        let model = make_dummy_model();
        assert_eq!(model.suggest("foobarrrr"), Correction::Incorrect);
    }

    #[test]
    fn unicode() {
        let model: CorrectorModel = vec!["janvier", "février", "novembre", "décembre"]
            .into_iter()
            .collect();

        use Correction::*;
        assert_eq!(model.suggest("January"), Incorrect);
        assert_eq!(model.suggest("fevrier"), Suggestion("février"));
        assert_eq!(model.suggest("November"), Suggestion("novembre"));
        assert_eq!(model.suggest("December"), Suggestion("décembre"));
    }
}
