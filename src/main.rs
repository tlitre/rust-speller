//! Command line based spell corrector.

use std::env;
use std::fs::File;
use std::io::{self, BufRead, BufReader, Write};
use std::process;

use correct_lib::*;

fn main() -> io::Result<()> {
    let filepath = env::args().nth(1).unwrap_or_else(|| {
        eprintln!(
            "usage: {} <corpus_file>",
            env::args()
                .next()
                .as_ref()
                .map(String::as_str)
                .unwrap_or("correct"),
        );
        process::exit(2);
    });

    let model = WordsFromBufRead::new(BufReader::new(File::open(filepath)?))
        .collect::<io::Result<CorrectorModel>>()?;

    interact(&model, io::stdin().lock(), io::stdout())
}

/// Interactively reads line by line, and prints the corrections in each line.
fn interact<R, W>(model: &CorrectorModel, reader: R, mut writer: W) -> io::Result<()>
where
    R: BufRead,
    W: Write,
{
    for line in reader.lines() {
        let line = line?;
        for word in Words::new(line) {
            use Correction::*;
            let correction = model.suggest(&word);
            match correction {
                Correct => writeln!(writer, "{}", word),
                Incorrect => writeln!(writer, "{}, -", word),
                Suggestion(suggested) => writeln!(writer, "{}, {}", word, suggested),
            }?
        }
    }
    Ok(())
}

/// Splits the given string into words, consuming the original string.
///
/// A _word_ is a sequence of alphabetic characters, may also contain one or more ascii apostropes
/// (U+0027 `'`) in the middle (but not at the beginning or the end).
#[derive(Debug)]
struct Words {
    text: String,
    next_index: usize,
}

impl Words {
    fn new(text: String) -> Self {
        Self {
            text,
            next_index: 0,
        }
    }
}

impl Iterator for Words {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {
        // XXX Using a good old while loop in order to track `next_index`.
        while self.next_index < self.text.len() {
            match self.text[self.next_index..]
                .split(|c: char| !c.is_alphabetic() && c != '\'')
                .next()
            {
                Some(token) => {
                    self.next_index += 1 + token.len();
                    let trimmed = token.trim_matches('\'');
                    if !trimmed.is_empty() {
                        return Some(String::from(trimmed));
                    } else {
                        continue;
                    }
                }
                None => {
                    self.next_index = self.text.len();
                    return None;
                }
            }
        }
        None
    }
}

/// Iterator for tokenizing words from BufRead.
#[derive(Debug)]
struct WordsFromBufRead<R> {
    lines: io::Lines<R>,
    words: Words,
}

impl<R: BufRead> WordsFromBufRead<R> {
    fn new(input: R) -> Self {
        Self {
            lines: input.lines(),
            words: Words::new(String::new()),
        }
    }
}

impl<R: BufRead> Iterator for WordsFromBufRead<R> {
    type Item = io::Result<String>;

    fn next(&mut self) -> Option<Self::Item> {
        loop {
            if let Some(word) = self.words.next() {
                return Some(Ok(word));
            }
            match self.lines.next() {
                None => return None,
                Some(Err(e)) => return Some(Err(e)),
                Some(Ok(line)) => {
                    self.words = Words::new(line);
                }
            }
        }
    }
}

#[cfg(test)]
mod interact_tests {
    use super::{interact, CorrectorModel, Words};

    fn model_from_str(corpus: &str) -> CorrectorModel {
        Words::new(String::from(corpus)).collect()
    }

    #[test]
    fn it_works() {
        let model = model_from_str("hello world hello word hello world");
        let input = "hello hell\nword\t  wordl wor wo w".as_bytes();
        let mut output = Vec::new();

        interact(&model, input, &mut output).unwrap();

        assert_eq!(
            "hello\nhell, hello\nword\nwordl, world\nwor, word\nwo, word\nw, -\n",
            String::from_utf8(output).unwrap()
        );
    }
}

#[cfg(test)]
mod words_tests {
    use super::{Words, WordsFromBufRead};

    use std::io::{self, BufReader};
    use std::iter::FromIterator;

    fn assert_words(expected: &[&str], text: &str) {
        assert_eq!(
            expected,
            Vec::from_iter(Words::new(String::from(text))).as_slice(),
        );
    }

    fn assert_buf_read(text: &str) {
        // Use `Words` to check `WordsFromBufRead`.
        let expected = Vec::from_iter(Words::new(String::from(text)));
        let result = WordsFromBufRead::new(BufReader::new(text.as_bytes()))
            .collect::<io::Result<Vec<_>>>()
            .unwrap();
        assert_eq!(expected, result);
    }

    #[test]
    fn it_works() {
        assert_words(&["hello", "world"], "\thello,  world!\n");
    }

    #[test]
    fn apostrope() {
        assert_words(
            &["The", "word", "is", "wouldn't"],
            "The word is ---    'wouldn't'",
        );
    }

    #[test]
    fn buf_read() {
        assert_buf_read("The\tquick\n\n\tbrown fox jumps\nover\tthe lazy   dog\n\n\n");
    }
}
