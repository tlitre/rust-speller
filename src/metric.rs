//! Distance metric for spelling correction.

use std::cmp;
use std::iter::FromIterator;
use std::mem;

/// Calculates the _restricted_ Damerau-Levenshtein distance between `s1` and `s2`.
///
/// With Damerau-Levenshtein distance, the following is considered an "edit" action:
///
/// - Insertion or deletion of one letter;
/// - Replacement of one letter;
/// - Transposition of two adjacent letters.
///
/// For simplicity and performance, the _restricted_ edit distance assumes that no substring is
/// edited more than once. (e. g. "CA" -> "ABC" is of distance 3 instead of 2)
/// See [Wikipedia](https://en.wikipedia.org/wiki/Damerau%E2%80%93Levenshtein_distance#Optimal_string_alignment_distance).
pub fn calculate_distance(s1: &str, s2: &str) -> usize {
    let n2 = s2.chars().count();

    let mut dists_last2 = vec![0; n2 + 1];
    let mut dists_last = Vec::from_iter(0..=n2);
    let mut dists = vec![0; n2 + 1];

    let mut c1_last = None;
    for (i1, c1) in (1..).zip(s1.chars()) {
        dists[0] = i1;

        let mut c2_last = None;
        for (i2, c2) in (1..).zip(s2.chars()) {
            // insert or delete
            let mut dist = cmp::min(dists[i2 - 1], dists_last[i2]) + 1;
            // replace or no-op
            assign_min(&mut dist, dists_last[i2 - 1] + (c1 != c2) as usize);
            // transposition
            if c1_last == Some(c2) && c2_last == Some(c1) {
                // c1_last is not None <==> dists_last2 is available
                // c2_last is Some <==> i2 >= 2
                assign_min(&mut dist, dists_last2[i2 - 2] + 1);
            }

            dists[i2] = dist;
            c2_last = Some(c2);
        }

        c1_last = Some(c1);
        mem::swap(&mut dists_last2, &mut dists_last);
        mem::swap(&mut dists_last, &mut dists);
    }
    dists_last[n2]
}

/// Assigns `value` to `variable` if `value` is less.
fn assign_min<T: PartialOrd>(variable: &mut T, value: T) {
    if value < *variable {
        *variable = value;
    }
}

#[cfg(test)]
mod calculate_distance_tests {
    use super::calculate_distance;

    fn assert_cases<'a>(it: impl IntoIterator<Item = &'a (usize, &'a str, &'a str)>) {
        for &(d, s1, s2) in it {
            // Ensuring symmetry.
            assert_eq!(d, calculate_distance(s1, s2), "{} -> {}", s1, s2);
            assert_eq!(d, calculate_distance(s2, s1), "{} <- {}", s1, s2);
        }
    }

    #[test]
    fn it_works() {
        assert_cases(&[(0, "", ""), (4, "1234", ""), (0, "1234", "1234")]);
    }

    #[test]
    fn edits() {
        assert_cases(&[
            // insert or delete
            (1, "abcde", "aabcde"),
            (1, "abcde", "abcdef"),
            (2, "abcde", "abccdee"),
            // insert + delete
            (2, "abcdefg", "acdehfg"),
            // replace
            (2, "abcdefg", "abhdefi"),
            // transposition
            (1, "abcde", "abdce"),
        ]);
    }

    #[test]
    fn more_edits() {
        assert_cases(&[
            (3, "abc", "cde"),
            (3, "bcd", "abcdef"),
            (4, "bcde", "acbedf"),
            (3, "ca", "abc"), // not 2 because we are using _restricted_ edit distance
            (2, "abcd", "acdb"), // 1 insertion + 1 deletion, not 2 transpositions
        ]);
    }
}
