# Spelling Corrector

An implementation of a spelling corrector in rust, currently designed to give the highest-frequency, lowestest-distance correction.

This program uses linear search over BK-tree or tries, building on [Wolf Garbe's findings](https://towardsdatascience.com/symspell-vs-bk-tree-100x-faster-fuzzy-string-search-spell-checking-c4f10d80a078)

We mainly chose this implementation to do our own benchmarking against the BK-tree implementations that were most popular in our class.

# Usage
This program needs to be called with a corpus to build the model with and then takes input from the command line.

`$ cargo run corpus.txt < input.txt`

Where corpus is a text file and input could be a list of words to correct or could be typed one at time into the cli.